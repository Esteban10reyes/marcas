<?php


namespace UPT;


class Celulares extends conexion
{


    public $id;
    public $nombre;
    public $marca;
    public $modelo;
    public $ram;
    public $procesador;
    public $semana;


    public function __construct()
    {
        parent::__construct();
    }
    //registra los datos en la base de datos
    function crear()
    {
        $pre = mysqli_prepare($this->con, "INSERT INTO registros(nombre,marca,modelo,ram,procesador,semana) VALUES(?,?,?,?,?,?)");
        $pre->bind_param("ssssss", $this->nombre, $this->marca, $this->modelo, $this->ram, $this->procesador, $this->semana);
        $pre->execute();
    }


    /*pruebas1*/


    static function contar($marca)
    {

        $me = new conexion();
        $pre = mysqli_prepare($me->con, "SELECT * FROM registros where marca=? ");
        $pre->bind_param("s", $marca);
        $pre->execute();
        $res = $pre->get_result();
        while ($y = mysqli_fetch_assoc($res)) {
            $t[] = $y;
        }

        if (isset($t)) {
            return $t;
        }

    }
    //Busca las fechas de la semana e imprime en una tabla
    static function buscar($semana)
    {
        $me = new conexion();
        $pre = mysqli_prepare($me->con, "SELECT * FROM registros where semana=? ");
        $pre->bind_param("s", $semana);
        $pre->execute();
        $res = $pre->get_result();
        while ($y = mysqli_fetch_assoc($res)) {
            $t[] = $y;
        }

        if (isset($t)) {
            return $t;
        }

    }
    //imprimir comparar uno y dos en la misma tabla e imprimir la de la ram mas grande.
    //-------------------------------Comparar-------------------------------------------------//
    static function comparar($modelo1,$modelo2)
    {
        $me = new conexion();
        $pre = mysqli_prepare($me->con, "SELECT * FROM registros where modelo=?");
        $pre->bind_param("s", $modelo1);
        $pre->execute();
        $res = $pre->get_result();
        while ($y = mysqli_fetch_assoc($res)) {
            $t[] = $y;
        }
        $me = new conexion();
        $pre = mysqli_prepare($me->con, "SELECT * FROM registros where modelo=?");
        $pre->bind_param("s", $modelo2);
        $pre->execute();
        $res = $pre->get_result();
        while ($y = mysqli_fetch_assoc($res)) {
            $t[] = $y;
        }
        if (isset($t)) {
            return $t;
        }


    }


}

