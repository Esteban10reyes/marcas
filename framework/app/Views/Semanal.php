<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sistema de Celulares</title>
    <link href="../../../../marcas/framework/public/css/EstilosSemanal.css" rel="stylesheet" type="text/css">
    <div id="principal">
        <header>
            <div id="logo">

                <center><h2> Sistema de Celulares</h2>
                    <p>Podras llevar un control exacto de tus equipos</p></center>
            </div>

            <nav class="menu">
                <ul>

                    <li><a href="http://localhost/marcas/framework/Index.php?controller=Celular&action=celulares">Regresar</a></li>
                </ul>
            </nav>
        </header>

    </div>
</head>
<body>

<div class="login-box">
    <div class="titulo"><h1>Buscar</h1> </div>

<form class="1" method="post" action="Index.php?controller=Celular&action=buscar">
    <ul>
        <div class="textbox">
            <i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-post" viewBox="0 0 16 16">
                    <path d="M4 3.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v8a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5v-8z"/>
                    <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z"/>
                </svg></i>
            <input type="text" placeholder="Semana" name="semana" required>
        </div>
        <input class="btn" type="submit" value="BUSCAR" </ul>

    <footer>
        <center> <p>
                (c)todos los derechos reservados -Sistemas de Celulares <br>
                Diseñado por Esteban Reyes y Alexis Miranda
            </p></center>
    </footer>
</div>

</form>

<div class="container-table">
    <div class="table-title">REGISTROS DE LA SEMANA</div>
    <div class="table__heder">ID</div>
    <div class="table__heder">NOMBRE</div>
    <div class="table__heder">MARCA</div>
    <div class="table__heder">MODELO</div>
    <div class="table__heder">RAM</div>
    <div class="table__heder">PROCESADOR</div>
    <div class="table__heder">Semana/mes</div>

    <?php
    if(isset($semanal)){
    foreach ($semanal as $dato){
    ?>
    <div class="table__item"><?php echo $dato["id_celular"];?></div>
    <div class="table__item"><?php echo $dato["nombre"];?></div>
    <div class="table__item"><?php echo $dato["marca"];?></div>
    <div class="table__item"><?php echo $dato["modelo"];?></div>
    <div class="table__item"><?php echo $dato["ram"];?></div>
    <div class="table__item"><?php echo $dato["procesador"];?></div>
    <div class="table__item"><?php echo $dato["semana"];?></div>
        <?php
    }
    }
    ?>

</div>
</table>

</body>
</html>
